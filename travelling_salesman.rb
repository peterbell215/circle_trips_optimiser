# frozen_string_literal: true

require 'bundler'
Bundler.require :default

dataset = Array.new(10) { CircleTripsOptimiser::Point.new(Random.rand * 10.0, Random.rand * 10.0) }

# exhaustive_search = RouteOptimizerExhaustiveSearch.new(dataset)
# exhaustive_search.search

genetic_search = CircleTripsOptimiser::RandomMutationSearch.new(dataset)
genetic_search.search
