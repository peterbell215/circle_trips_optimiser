# frozen_string_literal: true

module CircleTripsOptimiser
  # Class that searches for the shortest path using a genetic algorithm.
  class RandomMutationSearch
    POOL_SIZE = 10
    GENERATIONS = 100

    def initialize(points)
      @points = points
      @shortest_length = nil
      @shortest_path = nil
    end

    def search
      create_pool

      GENERATIONS.times do
        evolve_shortest_path
        evolve_random_path_based_on_length

        puts "@shortest_length = #{@shortest_length}, @shortest_path = #{@shortest_path}"
      end
    end

    private

    # Create a set of random paths based randoming shuffling the given set of points.
    def create_pool
      @pool = Array.new(POOL_SIZE) { Path.new(@points.shuffle) }

      first_sweep
      second_sweep
    end

    # First sweep: linear scan to calculate path lengths.  As we don't yet know the shortest path, some paths
    # will keep their length, rather than record the exceedance.
    def first_sweep
      @pool.each do |path|
        this_path_length = path.path_length(@shortest_length)
        if this_path_length != :longer_than_minimum_path
          @shortest_path = path
          @shortest_length = this_path_length
        end
      end
    end

    # In the second sweep we genuinely know what the shortest path is, so we recalculate the exceedances
    def second_sweep
      @pool.each { |path| path.path_length(@shortest_length) if path != @shortest_path }
    end

    # As well as evolving the shortest path, we also evolve one other path in the distribution.  The aim is to avoid
    # focusing on a local minimum at the expense of all other options.  We choose a longer path based on an exponential
    # distribution.  So we calculate a minimum exceedance for the second path as an exponential distribution between
    # in the range [0, length of vector].  We find the path who exceeds this random variable by the maximum amount.  We
    # evolve this solution.  This will favour paths that are a little longer than the minimum path.
    def evolve_random_path_based_on_length
      path_to_replace = randomized_selection_of_longer_path

      short_path_cut_off = 1.0-Math.log(1.0 - Random.rand) * 4.0
      path_to_evolve = @pool.max_by do |path|
        path != @path && path.exceeds_min_length_at < short_path_cut_off ? path.exceeds_min_length_at : 0
      end

      new_path = path_to_evolve.evolve
      path_to_replace.replace(new_path)

      first_sweep
      second_sweep
    end

    #
    def evolve_shortest_path
      path_to_replace = randomized_selection_of_longer_path
      path_to_replace.replace(@shortest_path.evolve)

      first_sweep
      second_sweep
    end

    # Applies a weighted randomized selection of which paths in the pool to be replaced.
    def randomized_selection_of_longer_path
      random_dice = Random.rand 0.._total_extras

      @pool.inject(0) do |extras_so_far, path|
        next extras_so_far if path == @shortest_path

        extras_so_far += path.exceeds_min_length_at
        return path if extras_so_far >= random_dice

        extras_so_far
      end
      @pool.last
    end

    def _total_extras
      @pool.inject(0) do |extras_so_far, path|
        extras_so_far += path.path_length(@shortest_length) == :longer_than_minimum_path ? path.exceeds_min_length_at : 0
      end
    end

    def first_path(path)
      @shortest_length = path.path_length
      @shortest_path = path
    end

    def subsequent_path(path)
      this_length = path.path_length(@shortest_length)

      return unless this_length != :longer_than_minimum_path && this_length < @shortest_length

      @shortest_length = this_length
      @shortest_path = path
    end
  end
end