# frozen_string_literal: true

module CircleTripsOptimiser
  # X, Y point on the plane.
  class Point
    def initialize(x, y)
      @x = x
      @y = y
    end

    def to_s
      "(#{x}, #{y})"
    end

    attr_reader :x, :y
  end
end
