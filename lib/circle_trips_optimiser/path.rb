# frozen_string_literal: true

require 'circle_trips_optimiser'

module CircleTripsOptimiser
  # Represents a path for a single path travelled by a vehicle.
  class Path < Array
    def initialize(*various_parameters)
      super
    end

    def path_length
      @path_length ||= _calculate_path_length
    end

    # Creates a new solution by swapping two randomly chosen segments of equal length over.
    # @return [CircleTripsOptimiser::Path] new copy of path with a swap over.
    def evolve
      first_cut, second_cut, length_of_swap_over = _swap_overs

      new_path = self.dup
      (0...length_of_swap_over).each do |offset|
        self[first_cut + offset], self[second_cut + offset] = self[second_cut + offset], self[first_cut + offset]
      end
      new_path
    end

    private

    # Generates a set of three numbers to swap over two segments of the solution.
    # @return [Integer, Integer, Integer] start of first cut, start of second cut, length of cut
    def _swap_overs
      max_for_1st_cut = self.length - 2
      first_cut = Random.rand 0..max_for_1st_cut

      end_of_second_cut = first_cut < max_for_1st_cut ? Random.rand(2..self.length - 1) : max_for_1st_cut

      length_of_swap_over = (end_of_second_cut - first_cut)/2
      length_of_swap_over = Random.rand 1..length_of_swap_over if length_of_swap_over > 1

      second_cut = end_of_second_cut - length_of_swap_over

      [ first_cut, second_cut, length_of_swap_over ]
    end

    def _calculate_path_length
      @path_length = 0
      prev = self.first
      self.drop(1).each_with_index do |p, index_at_path|
        @path_length += PointCache.instance.lookup(prev, p) do |prev, p|
          Math.hypot(prev.x - p.x, prev.y - p.y)
        end

        return :longer_than_minimum_path if _path_sofar_exceeds_min_length?(existing_min_length, index_at_path)
      end
      @path_length
    end
  end
end
