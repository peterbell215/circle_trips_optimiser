# frozen_string_literal: true

require 'singleton'

module CircleTripsOptimiser
  class PointCache
    include Singleton

    def initialize
      @cache = {}
    end

    def lookup(from, to)
      from_hash = (@cache[from.object_id] ||= {})
      from_hash[to.object_id] ||= yield(from, to)
    end
  end
end



