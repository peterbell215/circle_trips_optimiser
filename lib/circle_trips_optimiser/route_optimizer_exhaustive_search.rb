# frozen_string_literal: true

module CircleTripsOptimiser
  class RouteOptimizerExhaustiveSearch
    def initialize(points)
      @points = points
      @shortest_length = nil
      @shortest_path = nil
    end

    def search
      @points.permutation.each do |shuffled_points|
        path = Path.new(shuffled_points)

        @shortest_length.nil? ? first_path(path) : subsequent_path(path)
        puts "@shortest_length = #{@shortest_length}, @shortest_path = #{@shortest_path}"
      end
    end

    def first_path(path)
      @shortest_length = path.path_length
      @shortest_path = path
    end

    def subsequent_path(path)
      this_length = path.path_length(@shortest_length)

      return unless this_length != :longer_than_minimum_path && this_length < @shortest_length

      @shortest_length = this_length
      @shortest_path = path
    end
  end
end