# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)

$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require "circle_trips_optimiser/version"

Gem::Specification.new do |spec|
  spec.required_ruby_version = ">= 3.0.0"

  spec.name          = "circle_trips_optimiser"
  spec.version       = CircleTripsOptimiser::VERSION
  spec.authors       = ["trapeze-bell-peter"]
  spec.email         = ["peter.bell215@gmail.com"]

  spec.summary       = "Proof of concept for using ML algorithms to provide optimised trips for Circle"
  spec.description   = %(
    Designed to test some concepts around using Genetic Algorithms and the Schrimpf algorithm to generate
    a set of optimal trips for picking up pupils and taking them to a school.)
  spec.license       = "Internal - not for public release"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.2"
  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "rspec", "~> 3.11"
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency "yard"
end
