# frozen_string_literal: true

describe CircleTripsOptimiser::Path do
  describe '#evolve' do
    subject(:path) { CircleTripsOptimiser::Path.new(10) { |i| i } }

    context 'when default case' do
      before do
        expect(Random).to receive(:rand).exactly(3).times.and_return(0, 6, 2)
      end

      it 'switches the two segments' do
        path.evolve

        expect(path).to contain_exactly 0, 1, 6, 7, 4, 5, 2, 3, 8, 9
      end
    end

    context 'when the first cutover is two from the end' do
      before do
        expect(Random).to receive(:rand).once.and_return(8)
      end

      it 'switches the last two points' do
        path.evolve

        expect(path).to contain_exactly 0, 1, 2, 3, 4, 5, 6, 7, 9, 8
      end
    end
  end
end
