# frozen_string_literal: true

require 'circle_trips_optimiser'

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end